#pragma once

#include <iostream>
#include <fstream>
#include <math.h>
#define N 100
#define M 100

using namespace std;

namespace funct
{
	void read(int n, int m, int mas[N][M]);

	bool IsPrime(int x);

	int foundPrime(int n, int m, int mas[N][M], int prime[N]);


	bool maximum(int n, int m, int mas[N][M]);


	int sort(int n, int m, int mas[N][M], int prime[N]);

	void out_m(int n, int m, int mas[N][M]);
}