#include "../inc/Functions.hpp"

using namespace std;

namespace funct
{

    void read(int n, int m, int mas[N][M])
    {

        for (int i = 0;i < n;i++)
        {
            for (int j = 0;j < m;j++)
            {
                cin >> mas[i][j];
            }
        }
    }

    bool IsPrime(int x)
    {
        if (x < 2)
        {
            return false;
        }

        for (int d = 2; d <= sqrt(x); d++)
        {
            if (x % d == 0)
            {
                return false;
            }
        }

        return true;
    }

    int foundPrime(int n, int m, int mas[N][M], int prime[N])
    {
        for (int i = 0;i < n;i++)
        {
            for (int j = 0; j < m;j++)
            {
                if (IsPrime(mas[i][j]))
                    prime[i]++;

            }
        }

        for (int i = 0;i < n;i++)
        {
            return prime[i];
        }

    }


    bool maximum(int n, int m, int mas[N][M])
    {
        int max = INT_MIN;
        int index_i = 0;
        int index_j = 0;
        int count = 0;

        for (int i = 0; i < n;i++)
        {
            for (int j = 0;j < m;j++)
            {
                if (max < mas[i][j])
                {
                    max = mas[i][j];

                    index_i = i;
                    index_j = j;
                }
            }
        }

        for (int i = 0; i < n;i++)
        {
            for (int j = 0;j < m;j++)
            {
                if ((max == mas[i][j]) and ((i != index_i) or (j != index_j)))
                {
                    count += 1;
                }

            }
        }

        if (count != 0)
        {
            return true;
        }

        else
        {
            return false;
        }

    }

    int sort(int n, int m, int mas[N][M], int prime[N])
    {
        int primes[N];

        for (int i = 0; i < n;i++)
        {
            for (int j = i + 1;j < n;j++)
            {
                if (prime[i] < prime[j])
                {
                    swap(mas[i], mas[j]);
                    swap(prime[i], prime[j]);
                }
            }
        }

        for (int i = 0;i < n;i++)
        {
            for (int j = 0;j < m;j++)
            {
                return mas[i][j];
            }
        }

    }

    void out_m(int n, int m, int mas[N][M])
    {
        for (int i = 0;i < n;i++)
        {
            for (int j = 0;j < m;j++)
            {
                cout << mas[i][j] << " ";
            }
        }
    }
}
