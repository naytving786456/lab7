﻿#include <iostream>
#include "inc/Functions.hpp"
#define N 100
#define M 100

using namespace funct;

int main()
{   
    const int N_max = 100;
   
    int k, l;
    cin >> k >> l;

    int matrix[N][M];
    int isprime[N_max];

    read(k, l, matrix);
    foundPrime(k, l, matrix, isprime);

    if (maximum(k, l, matrix))
    { 
        sort(k, l, matrix, isprime);
    }


    else
    {
        cout << "0";
    }
    out_m(k, l, matrix);
}

